class Animation
{
    constructor(param)
    {
        this.maxTempo = 5;

        this.number = 0;
        this.tempo = 0;
        
    }
    
    update()
    {
        this.tempo++;
        if(this.tempo >= this.maxTempo)
        {
            this.tempo = 0;
            this.number++;
            if(this.number >= this.maxNumber)
            {
                this.number = 0;
            }
        }
    }

    draw(x, y)
    {
        ctx.drawImage(this.sprite,
            1 + this.number*(this.width+2),
            1,
            this.width,
            this.height,
            x-(this.width/2),
            y-(this.height),
            this.width,
            this.height);
    }

    changeOrientation(param)
    {
        this.animation = param.animationsList[param.action+"_"+param.drawOrientation]
        this.width = parseInt(this.animation.width);
        this.height = parseInt(this.animation.height);
        this.sprite = new Image()
        this.sprite.src = "Sprite/"+param.name+"_"+param.action+"_"+param.drawOrientation+".png";
        this.maxNumber = this.animation.number;
        return {"width":this.width, "height":this.height,"animationShootX":this.animation.x, "animationShootY":this.animation.y};
    }
}