//classe contenant toutes les informations sur une map

class Button
{
    constructor(param)
    {
        this.x = param.x;
        this.y = param.y;

        this.width = param.width;
        this.height = param.height;

        this.text = param.text;
        this.border = 2;

        this.font = "30px Arial"
        this.action = param.action;
        this.target = param.target;
    }
    update()
    {
        if(INPUT.clickMouseLeft)
        {
            if(INPUT.mouseX > this.x && INPUT.mouseX < this.x + this.width && INPUT.mouseY > this.y && INPUT.mouseY < this.y + this.height)
            {
                this.actionOnClick()
            }
        }
    }

    draw()
    {
        ctx.fillStyle="Black";
        ctx.fillRect(this.x, this.y, this.width, this.height);
        ctx.fillStyle="Gray";
        ctx.fillRect(this.x+this.border, this.y+this.border, this.width-this.border*2, this.height-this.border*2);
        ctx.font = this.font;
        var textWidth = ctx.measureText(this.text).width;
        ctx.fillStyle="Black"
		ctx.fillText(this.text, (this.x+(this.width/2))-textWidth/2, (this.y+(this.height/2)+10));
    }

    actionOnClick()
    {
        if(this.action == "changeGameClass")
        {
            ACTUALMAINCLASS.ChangeGameClass(this.target);
        }
    }
}