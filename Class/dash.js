class Dash
{
    constructor(x, y)
    {
        this.width = 25;
        this.height = 36;
        this.x = x;
        this.y = y+this.height/2;
        

        this.image = new Image()
        this.image.src = "sprite/Player_Dash.png"
        
    }

    wallCollision()
    {
        var wallList = ACTUALMAINCLASS.GetActualGameClass().map.wallList;
        for(var i in wallList)
        {
            if(this.isContactWall(wallList[i]))
            {
                return true;
            }
        }
        return false;
    }
    isContactWall(another)
    {
        return !(this.x - (this.width/2) > another.x*another.spriteSize + (another.spriteSize) ||
		    this.x + (this.width/2) < another.x*another.spriteSize ||
			this.y - (this.height) > another.y*another.spriteSize + (another.spriteSize) ||
			this.y < another.y*another.spriteSize)
    }

    draw()
    {
        ctx.drawImage(this.image, this.x-this.width/2, this.y-this.height, this.width,this.height)
    }
}