class Ennemi extends Entity
{
    constructor(param)
    {
        super(param);
        this.reloading = true;
        this.reloadingTime = 0;
        this.reloadingTimeMax = 200;
    }
    update()
    {
        if(!this.destroyActivate)
        {
            this.move();
        }
        if(this.canShoot)
        {
            if(this.reloading)
            {
                this.reloadingTime++
                if(this.reloadingTime >= this.reloadingTimeMax)
                {
                    this.reloading = false;
                    this.reloadingTime=0;
                }
            }
            else
            {
                this.reloading = true;
                var player = ACTUALMAINCLASS.GetActualGameClass().player;
                var shootPos = this.getShootPos();
                var shootAngle = this.shootAngle(player.x, player.y, shootPos.x, shootPos.y)
                ACTUALMAINCLASS.GetActualGameClass().map.shootList.push(new Shoot({"x": shootPos.x,"y": shootPos.y, "rotation": shootAngle, "speed":5}))
            }
        }
        super.update();
    }

    draw()
    {
        super.draw(); 
    }
}