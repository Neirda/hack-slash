class Entity
{
    constructor(param)
    {
        this.name = param.name;

        this.bottomX = param.x;
        this.bottomY = param.y;
        
        this.speed = param.speed;
        if(param.speed > 0)
        {
            this.speed = param.speed;
            this.speedTimeMax = 0;
        }
        else if(param.speed < 0)
        {
            this.speed = 1;
            this.speedTimeMax = -(param.speed-1);
        }
        else
        {
            this.speed = 0;
        }
        this.speedTime = 0

        this.canShoot = param.canShoot;

        this.pv = param.pv;

        this.fly = param.fly;

        this.isDestroy = false;

        this.animationsList = param.animation;

        var firstKey = "Walk_Right"

        if(this.animationsList != undefined)
        {
            var firstKey = Object.keys(this.animationsList)[0];
        }
        

        this.action = firstKey.split('_')[0]
        this.orientation = firstKey.split('_')[1];
        this.newAction = this.action;
        this.drawOrientation = this.orientation;
        this.newOrientation = this.orientation;

        this.changeAnimation();


        this.imageExplosion = new Image()
		this.imageExplosion.src = "Sprite/explosion.png"
        this.destroyActivate = false
		this.isDestroy = false
		this.countDestroy = 0
        this.countDestroyTotal = 25
        
        this.hitboxWidth = param.hitboxWidth
		this.hitboxHeight = param.hitboxHeight
    }
    
    update()
    {
        if(this.destroyActivate)
		{
			this.countDestroy++;
			if(this.countDestroy >= this.countDestroyTotal)
			{
				this.isDestroy = true
            }
            return
        }
        
        this.fall();

        this.x = this.bottomX;
        this.y = this.bottomY-(this.spriteHeight/2);


        if(this.action != this.newAction)
        {
            this.action = this.newAction
            this.changeAnimation()
        }
        else if(this.drawOrientation != this.newOrientation)
        {
            this.drawOrientation = this.newOrientation
            this.changeOrientation(this);
        }
        this.animationDraw.update();
    }

    draw()
    {
        if(this.destroyActivate)
        {
			ctx.drawImage(this.imageExplosion,Math.floor(this.countDestroy%5)*64,Math.floor(this.countDestroy/5)*64,64, 64, this.x-(32/2), this.y-(32/2), 32,32)
            return;
        }
        this.animationDraw.draw(this.bottomX, this.bottomY);
        var shootPos = this.getShootPos();
        /*
        if(shootPos != null)
        {
            ctx.fillStyle="red";
            ctx.fillRect(shootPos.x, shootPos.y, 2, 2);
        }
        */
    }

    changeAnimation()
    {
        this.animationDraw = new Animation(this)
        this.changeOrientation(this)
    }

    changeOrientation()
    {
        var temp = this.animationDraw.changeOrientation(this);
        this.spriteWidth = temp.width;
        this.spriteHeight = temp.height;
        this.animationShootX = temp.animationShootX;
        this.animationShootY = temp.animationShootY;
        this.x = this.bottomX;
        this.y = this.bottomY-(this.spriteHeight/2)
    }

    move()
    {
        this.speedTime++;
        if(this.speedTime >= this.speedTimeMax)
        {
            this.speedTime = 0
            var speed = this.speed
            if(this.orientation == "Left")
            {
                speed = -speed;
            }
            this.bottomX += parseInt(speed);
            var wallList = ACTUALMAINCLASS.GetActualGameClass().map.wallList;
            var temp = null
            for(var i in wallList)
            {
                if(this.isContactWall(wallList[i]))
                {
                    temp = wallList[i];
                }
            }
            if(temp != null )
            {
                if(this.bottomX < (temp.x*temp.spriteSize))
                {
                    this.bottomX = (temp.x*temp.spriteSize)- (this.hitboxWidth/2)-1
                }
                else
                {
                    this.bottomX = ((temp.x+1)*temp.spriteSize) + (this.hitboxWidth/2)+1
                }
                
            }

            
        }
    }
    getShootPos()
    {
        if(!(this.canShoot && this.animationShootX != null && this.animationShootY != null))
        {
            return null;
        }
        return({"x":(this.x-(this.spriteWidth/2))+this.animationShootX, "y":(this.y-(this.spriteHeight/2))+this.animationShootY})
    }

    shootAngle(targetX, targetY, shootPosX = null, shootPosY = null)
    {
        var shootPos = this.getShootPos();
        if(shootPosX == null)
        {
            shootPosX = shootPos.x;
        }
        if(shootPosY == null)
        {
            shootPosY = shootPos.y;
        }
        return (Math.atan2(targetY-shootPosY, targetX - shootPosX) * 180 / Math.PI);
    }
    isHit()
    {
        this.pv--;
        if(this.pv <= 0)
        {
            this.destroyActivate = true
        }
    }

    
    isContactWall(another)
    {
        return !(this.bottomX - (this.hitboxWidth/2) > another.x*another.spriteSize + (another.spriteSize) ||
		    this.bottomX + (this.hitboxWidth/2) < another.x*another.spriteSize ||
			this.bottomY - (this.hitboxHeight) > another.y*another.spriteSize + (another.spriteSize) ||
			this.bottomY < another.y*another.spriteSize)
    }

    fall()
    {
        
        if(this.fly)
        {
            return;
        }
        var wallList = ACTUALMAINCLASS.GetActualGameClass().map.wallList;
        var temp = null
        this.bottomY += 3;
        for(var i in wallList)
        {
            if(this.isContactWall(wallList[i]))
            {
                temp = wallList[i];
            }
        }
        
        if(temp != null)
        {
            this.bottomY = (temp.y*temp.spriteSize)-1
        }
    }
}