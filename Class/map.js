class Map
{
    constructor(param, modelEnnemiList)
    {
        this.imageFond = new Image()
        this.imageFond.src = "sprite/"+param.backgroundSprite;

        this.imageWall = new Image()
        this.imageWall.src = "sprite/"+param.wallSprite;

        this.modelEnnemiList = modelEnnemiList.modelEnnemiList;

        this.wallSize = 16
        this.wallList = []
        this.createWall(param.wall)

        this.ennemisList = []
        this.shootList = [];
    }
    update()
    {
        for(var i in this.ennemisList)
		{
			this.ennemisList[i].update()
        }
        var ennemisListTemp = this.ennemisList
		for(var i in ennemisListTemp)
		{
			if(ennemisListTemp[i].isDestroy)
			{
				this.ennemisList.splice(i, 1)
			}
        }
        for(var i in this.shootList)
		{
            
			this.shootList[i].update(ACTUALMAINCLASS.GetActualGameClass().player)
        }
        var shootListTemp = this.shootList
		for(var i in shootListTemp)
		{
			if(shootListTemp[i].destroy() || shootListTemp[i].isDestroy)
			{
				this.shootList.splice(i, 1)
			}
        }
        
    }

    draw()
    {
        this.drawBackground();
        for(var i in this.ennemisList)
		{
			this.ennemisList[i].draw()
        }
        for(var i in this.shootList)
		{
			this.shootList[i].draw()
        }
        for(var i in this.wallList)
        {
            ctx.drawImage(this.imageWall, this.wallList[i].x*this.wallSize, this.wallList[i].y*this.wallSize, this.wallSize,this.wallSize)
        }
    }
    drawBackground()
    {
        ctx.drawImage(this.imageFond,0,0,this.imageFond.width, this.imageFond.height, 0,0, WIDTH,HEIGHT)
    }

    createWall(param)
    {
        for(var i =0; i < 60; i++)
        {
            this.wallList.push({"x":i,"y":0, "spriteSize":this.wallSize})
            this.wallList.push({"x":i,"y":39, "spriteSize":this.wallSize})
        }
        for(var i =1; i < 39; i++)
        {
            this.wallList.push({"x":0,"y":i, "spriteSize":this.wallSize})
            this.wallList.push({"x":59,"y":i, "spriteSize":this.wallSize})
        }
        for(var i =1; i < 59; i++)
        {
            this.wallList.push({"x":i,"y":9, "spriteSize":this.wallSize})
            this.wallList.push({"x":i,"y":19, "spriteSize":this.wallSize})
            this.wallList.push({"x":i,"y":29, "spriteSize":this.wallSize})
        }
        for(var i in param)
        {
            this.wallList.push({"x":param[i].x,"y":param[i].y, "spriteSize":this.wallSize})
        }
    }

    addEnnemi(name, x, y)
    {
        var ennemi = this.findEnnemiByName(name);
        if(ennemi == null)
        {
            return;
        }
        ennemi.x = x;
        ennemi.y = y
        this.ennemisList.push(new Ennemi(ennemi))
    }
    findEnnemiByName(name)
    {
        for(var i in this.modelEnnemiList)
		{
            if(this.modelEnnemiList[i].name == name)
            {
                return this.modelEnnemiList[i];
            }
        }
        return null;
    }

}