class Mouse
{
    constructor(param)
    {
        this.image = new Image()
        this.image.src = "Sprite/Target.png"
        this.imageBlock = new Image()
        this.imageBlock.src = "Sprite/Target Block.png"
        this.imageReloading = new Image()
        this.imageReloading.src = "Sprite/Target Loading.png"
        this.canShoot = true;
        this.width = 32;
        this.height = 32;

        this.reloading = false;
        this.reloadingTime = 0;
        this.reloadingTimeMax = 0;
        this.reloadingDist = 50;
        this.reloadingAdd = 10;
        this.reloadingNerfShortRange = 5;

        this.rechargeDash = false;
        this.invoquerDash = false;
        this.dashDesign = null;
        this.dashTime = 0;
        this.dashTimeMax = 100;
        this.dashTimeReload = 100;
    }
    update(player, shootAngle)
    {
        this.canShoot = true
        if(player.action != "Idle" && player.orientation == "Right" && (shootAngle >=90 || shootAngle <=-90))
        {
            this.canShoot = false
        }
        if(player.action != "Idle" && player.orientation == "Left" && (shootAngle <=90 && shootAngle >=-90))
        {
            this.canShoot = false
        }
        if(this.reloading)
        {
            this.reloadingTime++;
            if(this.reloadingTime >= this.reloadingTimeMax)
            {
                this.reloading = false
                this.reloadingTime = 0
            }
        }
        
    }

    draw()
    {
        if(this.invoquerDash)
        {
            this.dashDesign.draw();
            return;
        }
        if(this.reloading)
        {
            var reloadtime = ((this.reloadingTime/this.reloadingTimeMax)*9)-1
            ctx.drawImage(this.imageReloading,1 + Math.trunc(reloadtime)*(this.width+2),1,this.width, this.height, INPUT.mouseX-(this.width/2), INPUT.mouseY-(this.height/2), this.width,this.height)
        }
        else
        {
            if(this.canShoot)
            {
                ctx.drawImage(this.image, INPUT.mouseX-(this.width/2), INPUT.mouseY-(this.height/2), this.width,this.height)
            }
            else
            {
                ctx.drawImage(this.imageBlock, INPUT.mouseX-(this.width/2), INPUT.mouseY-(this.height/2), this.width,this.height)
            }
        }
    }

    isShooting(player)
    {
        if(this.canShoot && INPUT.clickMouseLeft && !this.reloading)
        {
            this.reloading = true;
            var shootPos = player.getShootPos()
            var dist = this.distance(shootPos.x, shootPos.y, INPUT.mouseX, INPUT.mouseY);
            this.reloadingTime = 0;
            this.reloadingTimeMax = ((dist/this.reloadingDist)+this.reloadingNerfShortRange)+this.reloadingAdd;
            return true;
        }
        return false;
    }

    isDash(player)
    {
        if(this.rechargeDash)
        {
            this.dashTime++;
            if(this.dashTime >= this.dashTimeReload)
            {
                this.dashTime = 0
                this.rechargeDash = false
            }
        }
        else
        {
            if(INPUT.clickMouseRight)
            {
                if(!this.invoquerDash)
                {
                    this.invoquerDash = true
                    this.dashDesign = new Dash(INPUT.mouseX, INPUT.mouseY)
                    if(this.dashDesign.wallCollision())
                    {
                        this.dashDesign = null;
                        this.invoquerDash = false
                    }
                    
                }
                else
                {
                    this.dashTime++;
                    if(this.dashTime >= this.dashTimeMax)
                    {
                        this.dashTime = 0
                        this.rechargeDash = true
                        this.invoquerDash = false
                        return ({"x":this.dashDesign.x, "y":this.dashDesign.y})
                    }
                }
            }
            if(!INPUT.clickMouseRight && this.invoquerDash)
            {
                this.invoquerDash = false
                this.dashTime = 0
            }
                        
        }
        return null;
    }

    distance(x1, y1, x2, y2) {
        return Math.sqrt(this.sqr(y2 - y1) + this.sqr(x2 - x1));
    }
    sqr(a) {
        return a*a;
    } 
}