class Player extends Entity
{
    constructor(param)
    {
        super(param);
        this.shootList = [];
        this.mouse = new Mouse();
        
    }
    update()
    {
        if(this.isDestroy)
        {
            ACTUALMAINCLASS.ChangeGameClass("GameOver");
            return;
        }
        else if(this.destroyActivate)
        {
            super.update();
            return;
        }
        this.moving();


        var shootAngle = this.shootAngle(INPUT.mouseX, INPUT.mouseY, this.x, this.y)
        if(this.action == "Idle" && (shootAngle >=90 || shootAngle <=-90))
        {
            this.orientation = "Left";
        }
        if(this.action == "Idle" && (shootAngle <=90 && shootAngle >=-90))
        {
            this.orientation = "Right";
        }
        this.newOrientation = this.orientation
        var angle = 35
        if(shootAngle > angle && shootAngle < 180-angle)
        {
            this.newOrientation = this.orientation+"Down"
        }
        if(shootAngle < -angle && shootAngle > -180+angle)
        {
            this.newOrientation = this.orientation+"Up"
        } 
        this.mouse.update(this, shootAngle);


        if(this.mouse.isShooting(this))
        {
            var shootPos = this.getShootPos();
            shootAngle = this.shootAngle(INPUT.mouseX, INPUT.mouseY, shootPos.x, shootAngle.y)
            this.shootList.push(new Shoot({"x": shootPos.x,"y": shootPos.y, "rotation": shootAngle, "speed":20}))
        }
        var retour = this.mouse.isDash(this)
        if(retour != null)
        {
            this.bottomX = retour.x;
            this.bottomY = retour.y;
        }
        super.update();

        var ennemiesList = ACTUALMAINCLASS.GetActualGameClass().map.ennemisList;

        for(var i in this.shootList)
		{
            
			this.shootList[i].update(ACTUALMAINCLASS.GetActualGameClass().map.ennemisList)
        }
        var shootListTemp = this.shootList
		for(var i in shootListTemp)
		{
			if(shootListTemp[i].destroy() || shootListTemp[i].isDestroy)
			{
				this.shootList.splice(i, 1)
			}
        }

        for(var i in ennemiesList)
        {
            if(this.isContact(ennemiesList[i]))
            {
                this.isHit();
            }
        }
    }

    draw()
    {
        super.draw();
        for(var i in this.shootList)
		{
			this.shootList[i].draw()
		}
        this.mouse.draw();
    }

    moving()
    {
        var isMoving = false;
        if(ACTUALMAINCLASS.CheckIfActionIsValide("MoveRight"))
        {
            isMoving = true;
            if(this.orientation != "Right")
            {
                this.orientation = "Right";
            }
            else
            {
                this.move();
            }
        }
        if(ACTUALMAINCLASS.CheckIfActionIsValide("MoveLeft"))
        {
            isMoving = true;
            if(this.orientation != "Left")
            {
                this.orientation = "Left";
            }
            else
            {
                this.move();
            }
        }
        if(isMoving)
        {
            this.newAction = "Walk"
        }
        else
        {
            this.newAction = "Idle"
        }
    }
    isContact(ennemi)
    {
        return !(this.x - (this.hitboxWidth/2) > ennemi.x + (ennemi.hitboxWidth/2) ||
		    this.x + (this.hitboxWidth/2) < ennemi.x - (ennemi.hitboxWidth/2) ||
			this.y - (this.hitboxHeight/2) > ennemi.y + (ennemi.hitboxHeight/2) ||
			this.y + (this.hitboxHeight/2) < ennemi.y - (ennemi.hitboxHeight/2))
    }

}