class Shoot
{
    constructor(param)
    {

        this.image = new Image()
        this.image.src = "sprite/shoot.png"

        this.x=param.x
		this.y=param.y
        this.rotation=param.rotation
		this.speed=param.speed
		this.width=10
        this.height=5
        
        this.isDestroy=false
    }
    update(ennemiesList)
    {
		var wallList = ACTUALMAINCLASS.GetActualGameClass().map.wallList;
        for(var i in wallList)
        {
            this.isHitWall(wallList[i])
        }
		if(typeof ennemiesList === 'object')
		{
			this.isHit(ennemiesList)
		}
		for(var i in ennemiesList)
		{
			this.isHit(ennemiesList[i])
		}
        this.move()
	}
	
	draw()
	{
		ctx.save()
		ctx.translate(this.x, this.y)
		ctx.rotate((this.rotation)* Math.PI/180)
		ctx.drawImage(this.image, -this.width/2, -this.height/2, this.width,this.height)
		ctx.restore()
    }
    
    move()
	{
		this.x += this.speed * Math.cos(this.rotation* Math.PI / 180);
		this.y += this.speed * Math.sin(this.rotation* Math.PI / 180);
	}

    destroy()
	{
		if(this.x>WIDTH || this.x<0 || this.y>HEIGHT || this.y<0)
		{
			return true
		}
		return false
	}
	isHit(ennemi)
	{
		if (this.x > ennemi.x-(ennemi.hitboxWidth/2) &&
			this.x < ennemi.x+(ennemi.hitboxWidth/2) &&
			this.y > ennemi.y-(ennemi.hitboxHeight/2) &&
			this.y < ennemi.y+(ennemi.hitboxHeight/2))
		{
			this.isDestroy = true
			ennemi.isHit()
		}
		
	}
	isHitWall(wall)
	{
		if (this.x > wall.x*wall.spriteSize &&
			this.x < wall.x*wall.spriteSize+wall.spriteSize &&
			this.y > wall.y*wall.spriteSize &&
			this.y < wall.y*wall.spriteSize+wall.spriteSize)
		{
			this.isDestroy = true
		}
		
	}
}