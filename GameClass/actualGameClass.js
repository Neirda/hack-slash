class ActualGameClass extends ActualMainClass
{
    constructor()
    {
        super();
        
        //Liste des gameClass du projet
        this.GameClassList = [];
        this.GameClassList.push(new Game());
        this.GameClassList.push(new Menu());
        this.GameClassList.push(new GameOver());
        //this.ChangeGameClass("Game");
        this.ChangeGameClass("Menu");

        this.modelEnnemiList;

        //Indique l'action que doit effectuer une touche. La liste des touches se trouve dans Input.js
        this.actionLinkedWithKey = {
            "MoveLeft" : 81,//*Q
            "MoveRight" : 68,//*D
        }
    }

    // INIT

    init()
    {
        loadJSON();
    }
    endLoadingJson()
    {
        var mapList = [];
        for(var id in jsonValue) {
            if(jsonValue[id].type == "player")
            {
                var tempPlayerInfo = jsonValue[id]
            }
            if(jsonValue[id].type == "ennemi")
            {
                this.modelEnnemiList = jsonValue[id].list;
            }
            if(jsonValue[id].type == "map")
            {
                mapList.push(jsonValue[id])
            }
        }
        this.GameClassList[0].init(tempPlayerInfo, mapList, this.modelEnnemiList);
        endInit()
    }
    
    // FIN INIT

    update()
    {
        this.ActualGameClass.update();
    }

    draw()
    {
        this.ActualGameClass.draw();
    }

    ChangeGameClass(type)
    {
        this.ActualGameClass = this.GetGameClassByType(type);
    }


    CheckIfActionIsValide(action)
    {
        if(this.actionLinkedWithKey[action] == undefined)
        {
            return false;
        }
        return INPUT.getKeyStatus(this.actionLinkedWithKey[action]);
    }

    GetGameClassByType(type)
    {
        for(var id in this.GameClassList) {
            if(this.GameClassList[id].type == type)
            {
                return this.GameClassList[id];
            }
        }
    }

    GetActualGameClass()
    {
        return this.ActualGameClass;
    }
}