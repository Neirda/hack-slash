class Game extends GameClass
{
    constructor()
    {
        super();

        this.type = "Game";

        this.player
        this.mapList = []
        this.map
    }

    init(playerInfo, mapList, modelEnnemiList)
    {
        this.player = new Player(playerInfo)
        for(var i in mapList)
        {
            this.mapList = new Map(mapList[i], {"modelEnnemiList":modelEnnemiList});
        }
        if(typeof this.mapList === 'object')
		{
            this.map = this.mapList;
        }
        else
        {
            this.map = this.mapList[0];
        }
        /*
        this.map.addEnnemi("Tank", 100,480)
        this.map.addEnnemi("Crawler1", 50,480)
        this.map.addEnnemi("Crawler2", 0,480)
        this.map.addEnnemi("Floater", 100,280)
        */
        
    }
    update()
    {
        document.getElementById('canvas').style.cursor = "none";
        this.map.update();
        this.player.update();
    }
    draw()
    {
        super.draw();
        this.map.draw();
        this.player.draw();
        
    }

}