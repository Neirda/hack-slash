class GameOver extends GameClass
{
    constructor()
    {
        super();
        this.type = "GameOver";

        this.background = "gameOver.png"
        this.imageFond = new Image()
        this.imageFond.src = "Sprite/"+this.background
    }

    update()
    {
    }

    draw()
    {
        super.draw()
        ctx.drawImage(this.imageFond,0,0,this.imageFond.width, this.imageFond.height, 0,0, WIDTH,HEIGHT)
    }
}