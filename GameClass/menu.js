class Menu extends GameClass
{
    constructor()
    {
        super();
        this.type = "Menu";
        
        this.buttonList = []
        this.buttonList.push(new Button({"x":WIDTH-220,"y":HEIGHT-140,"width":200,"height":50, "text":"Play", "action":"changeGameClass", "target":"Game"}))
        this.buttonList.push(new Button({"x":WIDTH-220,"y":HEIGHT-70,"width":200,"height":50, "text":"Game Over", "action":"changeGameClass", "target":"GameOver"}))

        this.background = "backgroundMenu.jpg"
        this.imageFond = new Image()
        this.imageFond.src = "Sprite/"+this.background
    }
    
    update()
    {
        for(var id in this.buttonList)
		{
			this.buttonList[id].update()
		}
    }

    draw()
    {
        ctx.clearRect(0, 0, WIDTH, HEIGHT);
        ctx.drawImage(this.imageFond,0,0,this.imageFond.width, this.imageFond.height, 0,0, WIDTH,HEIGHT)
        for(var id in this.buttonList)
		{
			this.buttonList[id].draw()
        }
    }

}