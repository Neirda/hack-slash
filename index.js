//Squelette de l'application
window.onload = function()
{
	
    canvas = document.getElementById('canvas')
    canvas.width = WIDTH
    canvas.height = HEIGHT
	ctx = canvas.getContext("2d")
	ctx.font = "bold 20pt sans-serif"
	disableRightClickContextMenu(document.getElementById('canvas'));
	init();
	
}

//Gestion de l'appui des touches ou de la souris

document.onmousemove = function(mouse){
	var rect = canvas.getBoundingClientRect();
	INPUT.mouseX = mouse.x - rect.left
	INPUT.mouseY = mouse.y - rect.top
}
document.onmousedown = function(mouse){
	INPUT.onClick(mouse.button, true)
}
document.onmouseup = function(mouse){
	INPUT.onClick(mouse.button, false)
}
document.addEventListener("keydown", function(e) {
	INPUT.onKeyPress(e.keyCode, true);
})
document.addEventListener("keyup", function(e) {
	INPUT.onKeyPress(e.keyCode, false);
})

function disableRightClickContextMenu(element) {
	element.addEventListener('contextmenu', function(e) {
	  if (e.button == 2) {
		// Block right-click menu thru preventing default action.
		e.preventDefault();
	  }
	});
  }
	

//appelle la fonction qui charge tous les json
function init()
{
	INPUT = new Input();
	ACTUALMAINCLASS = new ActualGameClass();
	ACTUALMAINCLASS.init();
}

//recupere les fichiers json chargés
function endLoadingJson()
{
	ACTUALMAINCLASS.endLoadingJson();
}
//débute le cycle du jeu
function endInit()
{
	requestAnimationFrame(game);
}

//CYCLE DU JEU
function game()
{
	if(ctx==null) { return }
	update()
	draw()
	requestAnimationFrame(game)
}

function update()
{
	ACTUALMAINCLASS.update();
}

function draw()
{
	ACTUALMAINCLASS.draw();
}
