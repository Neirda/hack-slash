<!--Page de base de l'édition de level. Utilisation du php pour utiliser les includes et permettre de sauvegarder les fichiers json-->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Game</title>
        <script type="text/javascript" src="sources/jquery.js"></script>
        <link rel="stylesheet" type="text/css" href="css.css"> 
        <?php include "scriptJS.html";?>
    </head>
    <body>
        <div class="Screen">
            <h1 id="titre">Hack & Shoot</h1>
            <canvas id="canvas"></canvas>
        </div>
    </body>
</html>