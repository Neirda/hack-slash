class Input
{
    constructor()
    {
        this.clickMouseLeft = false;
        this.clickMouseRight = false;
        this.mouseX;
        this.mouseY;
        this.keysDown = {
            //32 : false,//*Space
            //37 : false,//*left
            //38 : false,//*bottom
            //39 : false,//*right
            //40 : false,//*top
            //66 : false,//*B
            68 : false,//*D
            81 : false,//*Q
            83 : false,//*S
            90 : false//*Z
            
        }
    }
    //Permet de savoir si un touche du tableau est appuyée
    getKeyStatus(keyCode)
    {
        if(this.keysDown[keyCode] == undefined)
        {
            return false;
        }
        return this.keysDown[keyCode]
    }

    //Permet de changer une valeur du tableau en fonction des EventListener dans index.js
    onKeyPress(keyCode, value)
    {
       this.keysDown[keyCode] = value;
    }
    //Permet de changer une valeur de click de souris en fonction des EventListener dans index.js
    onClick(mouseButton, value)
    {
        if(mouseButton == 0)
        {
            this.clickMouseLeft = value;
        }
        if(mouseButton == 2)
        {
            this.clickMouseRight = value;
        }
    }
}