//permet de charger tous les fichiers Js souhaités
class FileList
{
    constructor(fileList)
    {
        this.files = fileList;
    }
}

var jsonValue = []

//charge le fichier files.json, contenant la liste des autres fichiers json a charger
function loadJSON() {
    $.getJSON("json/files.json", function( data ) {
        fileList = new FileList(data.files);
        loadingJSON(fileList.files, 0)
    });
}

//charge les autres fichiers json
function loadingJSON(tab, id) {
    $.getJSON("json/"+tab[id], function( data ) {
        jsonValue.push(data)
        if(id+1 < tab.length)
        {
            loadingJSON(tab, id+1)
        }
        else
        {
            loadingOther();
        }
    });
}

//si il y a autre chose a charger, sinon, revient aux fonctions dans index.js
function loadingOther() {
    endLoadingJson();
}